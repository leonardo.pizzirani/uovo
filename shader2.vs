
#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aColor;
out vec3 ourColor;
const float pi=3.1415926538f;
uniform float timeValue;
void main(){
   gl_Position = vec4(aPos.x, -1.0*aPos.y, aPos.z, 1.0f);
   ourColor = vec3(sin(aColor.x + timeValue + pi*2.0f/3.0f), sin(aColor.y + timeValue + pi*4.0f/3.0f), sin(aColor.z + pi*2.0f));
};
