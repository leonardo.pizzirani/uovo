#version 330 core
out vec4 FragColor;
in vec3 ourColor;
in vec3 vPos;
void main(){
   FragColor = vec4((vPos.xy+1.0f)/2.0f, vPos.z, 1.0f);
};
